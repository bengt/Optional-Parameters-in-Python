import pytest
from project import (CheckedProject, Project, StartAfterEndError,
                     StartEqualsEndError, UnderdefinedError,
                     ValueMismatchError, ZeroDurationError)


class TestCheckedProject(object):

    def test_zero_duration(self):
        duration = 0

        with pytest.raises(ZeroDurationError):
            _ = CheckedProject(duration=duration)

    def test_only_start(self):
        start = 1

        with pytest.raises(UnderdefinedError):
            _ = CheckedProject(start=start)

    def test_only_duration(self):
        duration = 1

        with pytest.raises(UnderdefinedError):
            _ = CheckedProject(duration=duration)

    def test_only_end(self):
        end = 1

        with pytest.raises(UnderdefinedError):
            _ = CheckedProject(end=end)

    def test_start_after_end(self):
        start = 2
        end = 1

        with pytest.raises(StartAfterEndError):
            _ = CheckedProject(start=start, end=end)

    def test_start_equals_end(self):
        start = 1
        end = 1

        with pytest.raises(StartEqualsEndError):
            _ = CheckedProject(start=start, end=end)

    def test_value_mismatch(self):
        start = 0
        duration = 1
        end = 2

        with pytest.raises(ValueMismatchError):
            _ = CheckedProject(start=start, duration=duration, end=end)

    def test_super_instanciation(self):
        start = 1
        duration = 2
        end = 3

        project = CheckedProject(start=start, duration=duration, end=end)

        assert project.start == 1
        assert project.duration == 2
        assert project.end == 3


class TestProject(object):

    def test_no_start(self):
        duration = 2
        end = 3

        project = Project(duration=duration, end=end)

        assert project.start == 1
        assert project.duration == 2
        assert project.end == 3

    def test_no_duration(self):
        start = 1
        end = 3

        project = Project(start=start, end=end)

        assert project.start == 1
        assert project.duration == 2
        assert project.end == 3

    def test_no_end(self):
        start = 1
        duration = 2

        project = Project(start=start, duration=duration)

        assert project.start == 1
        assert project.duration == 2
        assert project.end == 3

    def test_overspecified(self):
        start = 1
        duration = 2
        end = 3

        project = Project(start=start, duration=duration, end=end)

        assert project.start == 1
        assert project.duration == 2
        assert project.end == 3
