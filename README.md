# Optional Parameters in Python

Demonstration of an object where two parameters suffice to determine the third
one. In this example a project has an start, duration and end, where `end -
duration == start`. The object can be instanciated with any two of these
parameters given. The third parameter gets derived from the others on demand
using a property. A wrapper object extends the project object with some sanity
checks. The module is accompanied with an illustrative test suite which provides
100 % branch coverage. Tests can be run with tox and support Python 2.7, Python 3.5 and PyPy2 from a single code base.

## Clone

    $ git clone git@github.com:Bengt/Optional-Parameters-in-Python.git
    $ cd Optional-Parameters-in-Python/

## Run Tests

    $ tox
    $ xdg-open htmlcov/index.html

## Usage

```python
from project import Project

start = 1
end = 3

project = Project(start=start, end=end)

assert project.duration == 2
```

## Basic Idea

```python
class Project(object):
    [...]
    @property
    def duration(self):
        if self._duration is None:
            return self._end - self._start
        return self._duration
```
