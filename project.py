class ZeroDurationError (Exception):
    pass


class StartAfterEndError (Exception):
    pass


class ValueMismatchError (Exception):
    pass


class UnderdefinedError(Exception):
    pass


class StartEqualsEndError(Exception):
    pass


class Project (object):

    def __init__(self, start=None, duration=None, end=None):
        """
        At least two of the project's start, duration and end must be given.
        The third attribute will be derived from the given ones, if necessary.
        """

        self._start = start
        self._duration = duration
        self._end = end

    @property
    def start(self):
        if self._start is None:
            return self._end - self._duration
        return self._start

    @property
    def duration(self):
        if self._duration is None:
            return self._end - self._start
        return self._duration

    @property
    def end(self):
        if self._end is None:
            return self._start + self._duration
        return self._end


class CheckedProject(Project):

    def __init__(self, start=None, duration=None, end=None):
        """
        Extends Project with sanity checks for input parameters.
        """
        if duration is not None:
            if duration == 0:
                raise ZeroDurationError("Duration must be non-zero.")

        if [start, duration, end].count(None) > 1:
            raise UnderdefinedError(
                'At least two of start, duration and end must be defined.')

        if None not in [start, end]:
            if start > end:
                raise StartAfterEndError("End must follow start.")
            if start == end:
                raise StartEqualsEndError("Start and end must be non-equal.")

        if None not in [start, duration, end]:
            if end - start != duration:
                raise ValueMismatchError(
                    'Duration must be the difference of end and start.')

        super(CheckedProject, self).__init__(
            start=start, duration=duration, end=end)
